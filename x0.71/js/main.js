function notifyOpen() {
	$(".icon3").removeClass("invisible");
	$(".icon3-close").removeClass("visible");
	$(".settings").removeClass("visible");
	$(".icon2").removeClass("invisible");
	$(".icon2-close").removeClass("visible");
	$(".tools").removeClass("visible");

	$(".icon1").addClass("invisible");
	$(".icon1-close").addClass("visible");
	$(".notify").addClass("visible");
}

function notifyClose() {
	$(".icon1").removeClass("invisible");
	$(".icon1-close").removeClass("visible");
	$(".notify").removeClass("visible");
}

function toolsOpen() {
	$(".icon3").removeClass("invisible");
	$(".icon3-close").removeClass("visible");
	$(".settings").removeClass("visible");
	$(".icon1").removeClass("invisible");
	$(".icon1-close").removeClass("visible");
	$(".notify").removeClass("visible");

	$(".icon2").addClass("invisible");
	$(".icon2-close").addClass("visible");
	$(".tools").addClass("visible");
}

function toolsClose() {
	$(".icon2").removeClass("invisible");
	$(".icon2-close").removeClass("visible");
	$(".tools").removeClass("visible");
}

function settingsOpen() {
	$(".icon2").removeClass("invisible");
	$(".icon2-close").removeClass("visible");
	$(".tools").removeClass("visible");
	$(".icon1").removeClass("invisible");
	$(".icon1-close").removeClass("visible");
	$(".notify").removeClass("visible");

	$(".icon3").addClass("invisible");
	$(".icon3-close").addClass("visible");
	$(".settings").addClass("visible");
}

function settingsClose() {
	$(".icon3").removeClass("invisible");
	$(".icon3-close").removeClass("visible");
	$(".settings").removeClass("visible");
}
